#include "Level.hpp"
#include "Player.hpp"
#include "Ball.hpp"


#include "Engine.hpp"
#include "Event.hpp"
#include "primitives/Line.hpp"
#include "primitives/Sphere.hpp"

int main()
{
    kawe::Engine engine{};

    entt::registry *my_world;
    std::shared_ptr<cbk::Level> level;
    std::shared_ptr<cbk::Player> player;
    std::shared_ptr<cbk::Ball> ball;

    engine.on_create = [&my_world, &level, &player, &ball](entt::registry &world) {
        my_world = &world;

        player = std::make_shared<cbk::Player>(world);
        ball = std::make_shared<cbk::Ball>(world);
        level = std::make_shared<cbk::Level>(world, player->getEntity(), ball->getEntity());

        world.ctx<entt::dispatcher *>()->sink<kawe::event::TimeElapsed>()
            .connect<&cbk::Level::gameLogic>(level.get());

        world.ctx<entt::dispatcher *>()->sink<kawe::event::Pressed<kawe::event::Key>>()
            .connect<&cbk::Player::on_key_pressed>(player.get());

        world.ctx<entt::dispatcher *>()->sink<kawe::event::Released<kawe::event::Key>>()
            .connect<&cbk::Player::on_key_released>(player.get());

        my_world->ctx<kawe::State *>()->clear_color = {0.2f, 0.2f, 0.2f, 1.f};

    };

    engine.on_imgui = [&my_world]() {
        ImGui::Begin("Cute Breaker - Control Panel");
        const auto &in = my_world->ctx<kawe::State *>()->clear_color;
        float temp[4] = {in.r, in.g, in.b, in.a};

        if (ImGui::ColorEdit4("clear color", temp))
            my_world->ctx<kawe::State *>()->clear_color = {temp[0], temp[1], temp[2], temp[3]};

        ImGui::End();
    };

    engine.start();
}

#pragma once

#include "Engine.hpp"
#include "Event.hpp"

using namespace entt::literals;

namespace cbk {

class Player {
public:
    struct Size {
        double x;
        double y;
        double z;
    };

public:
    Player(entt::registry &world) : m_World{world}, m_Size{20.f, 10.f, 2.f}, m_Position{66.f, 5.f, 70.f}
    {
        m_Entity = m_World.create();

        m_World.emplace<kawe::Name>(m_Entity, "Player");

        kawe::Mesh::emplace(m_World, m_Entity, "./asset/models/cube/cube.obj");
        m_World.emplace<kawe::FillColor>(m_Entity, glm::vec4(.5f, .5f, .5f, .5f));
        // Fill the color need to enable shader default to work on model
        m_World.patch<kawe::Render::VAO>(m_Entity, [this](kawe::Render::VAO &component) {
            component.shader_program = m_World.ctx<kawe::State *>()->shaders[0].get();
        });

        m_World.emplace<Size>(m_Entity, m_Size);
        m_World.emplace<kawe::Scale3f>(m_Entity, glm::vec3(m_Size.x*0.5, m_Size.y*0.5, m_Size.z*0.5));
        m_World.emplace<kawe::Position3f>(m_Entity, m_Position);
        m_World.emplace<kawe::Velocity3f>(m_Entity, glm::vec3(0.f));

        m_World.emplace<kawe::Collider>(m_Entity, kawe::Collider());

        m_World.emplace<entt::tag<"Player"_hs>>(m_Entity);
        // m_World.emplace<entt::tag<"BounceZ"_hs>>(m_Entity);
    }

    auto getEntity() const { return m_Entity; }

    auto on_key_pressed(const kawe::event::Pressed<kawe::event::Key> &key)
    {
        std::optional<double> x{};
        std::optional<double> y{};
        std::optional<double> z{};

        switch (key.source.keycode) {
        case kawe::event::Key::Code::KEY_UP: y = 15; break;
        case kawe::event::Key::Code::KEY_LEFT: x = -15; break;
        case kawe::event::Key::Code::KEY_RIGHT: x = 15; break;
        case kawe::event::Key::Code::KEY_DOWN: y = -15; break;
        // case kawe::event::Key::Code::KEY_SPACE: y = 3.0; break;
        default: break;
        }
        if (x.has_value() || y.has_value() || z.has_value()) {
            const auto default_v =
                kawe::Velocity3f{glm::dvec3{x.value_or(0.0), y.value_or(0.0), z.value_or(0.0)}};
            const auto vel = m_World.try_get<kawe::Velocity3f>(m_Entity);

            if (vel == nullptr) {
                m_World.emplace_or_replace<kawe::Velocity3f>(m_Entity, default_v);
            } else {
                m_World.emplace_or_replace<kawe::Velocity3f>(
                    m_Entity,
                    glm::dvec3{
                        x.has_value() ? x.value() : vel->component.x,
                        y.has_value() ? y.value() : vel->component.y,
                        z.has_value() ? z.value() : vel->component.z,
                    });
            }
        }
    }

    auto on_key_released(const kawe::event::Released<kawe::event::Key> &key)
    {
        std::optional<double> x{};
        std::optional<double> y{};
        std::optional<double> z{};

        switch (key.source.keycode) {
        case kawe::event::Key::Code::KEY_UP: y = 0; break;
        case kawe::event::Key::Code::KEY_LEFT: x = 0; break;
        case kawe::event::Key::Code::KEY_RIGHT: x = 0; break;
        case kawe::event::Key::Code::KEY_DOWN: y = 0; break;
        // case kawe::event::Key::Code::KEY_SPACE: y = 3.0; break;
        default: break;
        }
        if (x.has_value() || y.has_value() || z.has_value()) {
            const auto vel = m_World.try_get<kawe::Velocity3f>(m_Entity);
            const auto newVelocity = kawe::Velocity3f{glm::dvec3{
                x.value_or(vel->component.x), y.value_or(vel->component.y), z.value_or(vel->component.z)}};
            m_World.emplace_or_replace<kawe::Velocity3f>(m_Entity, newVelocity);
        }
    }

private:
    entt::entity m_Entity;
    entt::registry &m_World;
    Size m_Size;
    glm::vec3 m_Position;
};

} // namespace cbk

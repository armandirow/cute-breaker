#pragma once

#include <cstdlib>
#include <ctime>

#include "Engine.hpp"
#include "Event.hpp"

#include "Player.hpp"
#include "Ball.hpp"
#include "Brick.hpp"
#include "LevelContainer.hpp"
#include "primitives/Sphere.hpp"

using namespace entt::literals;

namespace cbk {

constexpr float NB_LAYERS = 4.f;
constexpr float NB_LINES = 4.f;
constexpr float NB_COLUMNS = 4.f;

// TODO: convert this class into a component.
class Level {
public:
    Level(entt::registry &world, const entt::entity &player, const entt::entity &ball) :
        m_World{world}, m_Player{player}, m_Ball{ball},
        m_LevelContainer(
            world,
            glm::vec3(NB_COLUMNS * 10.3f, NB_LAYERS * 10.3f, 130.f + (10.3f * NB_LINES)),
            glm::vec3(0.f, 0.f, -40.f + (-10.3f * NB_LINES))),
        m_Score{0}
    {
        std::srand(static_cast<unsigned int>(std::time(nullptr)));

        m_Level = m_World.create();
        m_World.emplace<kawe::Name>(m_Level, "Level");

        for (float layer = 0.f; layer < NB_LAYERS; layer++) {
            for (float line = 0.f; line < NB_LINES; line++) {
                for (float collumn = 0.f; collumn < NB_COLUMNS; collumn++) {
                    m_Bricks.push_back(Brick(
                        m_World,
                        "Brick " + std::to_string(12 * line + collumn),
                        glm::vec3(collumn * 10.3f, layer * 10.3f, -20.f + (line * -10.3f)),
                        glm::vec3(10.f, 10.f, 10.f)));
                }
            }
        }

        std::vector<entt::entity> bricksEntities;
        for (auto &brick : m_Bricks) { bricksEntities.push_back(brick.getEntity()); }
        kawe::Children::emplace(m_World, m_Level, bricksEntities);

        m_World.patch<kawe::Position3f>(m_Player, [](auto &pos) {
            pos.component.x = (NB_COLUMNS * 10.3f) / 2.f;
            pos.component.y = (NB_LAYERS * 10.3f) / 2.f;
            pos.component.z = -40.f + (-10.3f * NB_LINES) + 130.f + (NB_LAYERS * 10.3f) - 20.f;
        });
        m_World.patch<kawe::Position3f>(m_Ball, [](auto &pos) {
            pos.component.x = (NB_COLUMNS * 10.3f) / 2.f;
            pos.component.y = (NB_LAYERS * 10.3f) / 2.f;
            pos.component.z = -40.f + (-10.3f * NB_LINES) + 130.f + (NB_LAYERS * 10.3f) - 40.f;
        });

        for (auto &camera : m_World.view<kawe::CameraData>()) {
            const auto &cameraData = m_World.get<kawe::CameraData>(camera);
            m_World.patch<kawe::CameraData>(camera, [](auto &cam) {
                cam.fov = 90;
            });
            m_World.patch<kawe::Position3f>(camera, [](auto &pos) {
                pos.component.x = 20;
                pos.component.y = 35;
                pos.component.z = 100;
            });
            m_World.patch<kawe::Position3f>(cameraData.target, [](auto &pos) {
                pos.component.x = 15;
                pos.component.y = -4;
                pos.component.z = 40;
            });

        }
    }

    auto breakWall(const entt::entity &brickSide) -> void
    {
        const auto &brick = m_World.try_get<kawe::Parent>(brickSide);
        if (!brick) return;

        const float ballDamage = 10.f;
        const auto &life = m_World.try_get<Brick::LifePoint>(brick->component);

        if (!life) return;

        m_World.patch<Brick::LifePoint>(brick->component, [&ballDamage](auto &l) { l.health -= ballDamage; });
        if (life->health > 0) return;
        const auto &sides = m_World.try_get<kawe::Children>(brick->component);
        if (!sides) return;


        spdlog::warn("Remove Brick {} {}", brick->name, brick->component);
        std::for_each(sides->component.begin(), sides->component.end(), [this, &brick](const auto &side) {
            spdlog::warn("Remove Brick Side {}", side);
            const auto &guizmos = m_World.try_get<kawe::Children>(side);
            if (guizmos) {
                std::for_each(guizmos->component.begin(), guizmos->component.end(), [this](const auto &guizmo) {
                    spdlog::warn("Remove Brick Side Guizmo {}", guizmo);
                    m_World.remove_all(guizmo);
                });
            }
            m_World.remove_all(side);
        });

        m_World.remove_all(brick->component);
        m_Score += 10;
    }

    auto playerBounce() -> void
    {
        for (const auto &player : m_World.view<entt::tag<"Player"_hs>>()) {
            const auto &playerColider = m_World.try_get<kawe::Collider>(player);
            if (!playerColider || playerColider->step != kawe::Collider::CollisionStep::AABB) return;
            const auto &playerPos = m_World.try_get<kawe::Position3f>(player);
            const auto &playerSize = m_World.try_get<kawe::Position3f>(player);
            const auto &ballPos = m_World.try_get<kawe::Position3f>(m_Ball);
            if (!playerPos || !playerSize || !ballPos) return;

            m_World.patch<kawe::Velocity3f>(m_Ball, [&playerPos, &playerSize, &ballPos](auto &ballVel) {
                ballVel.component.z = ballVel.component.z < 0 ? ballVel.component.z : -ballVel.component.z;

                // Change ballVel.x from 22.5° to +157.5°
                // Base on how far the ball is from the center of player
                // Ratio will in range [-player.x, player.x]
                const auto ratioX =
                    ((ballPos->component.x - playerPos->component.x) / playerSize->component.x) * 4;
                ballVel.component.x = ratioX * -ballVel.component.z;

                // Change ballVel.y from 22.5° to +157.5°
                // Base on how far the ball is from the center of player
                // Ratio will in range [-player.y, player.y]
                const auto ratioY =
                    ((ballPos->component.y - playerPos->component.y) / playerSize->component.y) * 4;
                ballVel.component.y = ratioY * ballVel.component.z;
            });

            const auto &ballVelocity = m_World.try_get<kawe::Velocity3f>(m_Ball);
            if (!ballVelocity) return;
            m_World.patch<kawe::Position3f>(
                m_Ball, [&ballVelocity](auto &pos) { pos.component.z += ballVelocity->component.z * 0.1; });
        }
    }

    auto gameLogic(const kawe::event::TimeElapsed &e) -> void
    {
        const auto dt_nano = e.elapsed;
        [[maybe_unused]] const auto dt_secs =
            static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(dt_nano).count())
            / 1'000'000.0;

        // BOUNCE LOGIC
        const auto &ballCollider = m_World.try_get<kawe::Collider>(m_Ball);
        if (ballCollider && ballCollider->step == kawe::Collider::CollisionStep::AABB) {
            for (const auto &bounce : m_World.view<entt::tag<"BounceX"_hs>>()) {
                const auto &bounceColider = m_World.try_get<kawe::Collider>(bounce);
                if (bounceColider && bounceColider->step == kawe::Collider::CollisionStep::AABB) {
                    m_World.patch<kawe::Collider>(
                        bounce, [](auto &c) { c.step = kawe::Collider::CollisionStep::NONE; });
                    m_World.patch<kawe::Velocity3f>(m_Ball, [&bounce](auto &vel) {
                        spdlog::info("BounceX {} Velocity.x {}", bounce, vel.component.x);
                        vel.component.x *= -1;
                    });
                    const auto &velocity = m_World.try_get<kawe::Velocity3f>(m_Ball);
                    if (velocity) {
                        spdlog::info("New Velocity.x {}", velocity->component.x);
                        m_World.patch<kawe::Position3f>(
                            m_Ball, [&velocity](auto &pos) { pos.component.x += velocity->component.x * 0.1; });
                    }
                    breakWall(bounce);
                    break;
                }
            }
            for (const auto &bounce : m_World.view<entt::tag<"BounceY"_hs>>()) {
                const auto &bounceColider = m_World.try_get<kawe::Collider>(bounce);
                if (bounceColider && bounceColider->step == kawe::Collider::CollisionStep::AABB) {
                    m_World.patch<kawe::Collider>(
                        bounce, [](auto &c) { c.step = kawe::Collider::CollisionStep::NONE; });
                    m_World.patch<kawe::Velocity3f>(m_Ball, [&bounce](auto &vel) {
                        spdlog::info("BounceY {} Velocity.y {}", bounce, vel.component.y);
                        vel.component.y *= -1;
                    });
                    const auto &velocity = m_World.try_get<kawe::Velocity3f>(m_Ball);
                    if (velocity) {
                        spdlog::info("New Velocity.y {}", velocity->component.y);
                        m_World.patch<kawe::Position3f>(
                            m_Ball, [&velocity](auto &pos) { pos.component.y += velocity->component.y * 0.1; });
                    }
                    breakWall(bounce);
                    break;
                }
            }
            for (const auto &bounce : m_World.view<entt::tag<"BounceZ"_hs>>()) {
                const auto &bounceColider = m_World.try_get<kawe::Collider>(bounce);
                if (bounceColider && bounceColider->step == kawe::Collider::CollisionStep::AABB) {
                    m_World.patch<kawe::Collider>(
                        bounce, [](auto &c) { c.step = kawe::Collider::CollisionStep::NONE; });
                    m_World.patch<kawe::Velocity3f>(m_Ball, [&bounce](auto &vel) {
                        spdlog::info("BounceZ {} Velocity.z {}", bounce, vel.component.z);
                        vel.component.z *= -1;
                    });
                    const auto &velocity = m_World.try_get<kawe::Velocity3f>(m_Ball);
                    if (velocity) {
                        spdlog::info("New Velocity.z {}", velocity->component.z);
                        m_World.patch<kawe::Position3f>(
                            m_Ball, [&velocity](auto &pos) { pos.component.z += velocity->component.z * 0.1; });
                    }
                    breakWall(bounce);
                    break;
                }
            }

            // PLAYER BOUNCE
            playerBounce();
        }
    }

private:
    entt::registry &m_World;
    entt::entity m_Player;
    entt::entity m_Ball;
    LevelContainer m_LevelContainer;
    entt::entity m_Level;
    unsigned int m_Score;

    std::vector<Brick> m_Bricks;
};

} // namespace cbk

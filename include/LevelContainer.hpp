#pragma once

#include "Engine.hpp"

using namespace entt::literals;

namespace cbk {

class LevelContainer {
public:
    LevelContainer(entt::registry &world, const glm::vec3 &dim, const glm::vec3 &pos) : m_World{world}, m_Dim(dim), m_Pos(pos)
    {
        m_LevelContainer = m_World.create();
        m_World.emplace<kawe::Name>(m_LevelContainer, "Level Container");

        m_Top = addCube(
            "Top Container",
            glm::vec3(m_Pos.x + m_Dim.x * 0.5f, m_Pos.y + m_Dim.y + 1.1f, m_Pos.z + m_Dim.z * 0.5f),
            glm::vec3(m_Dim.x, 1.f, m_Dim.z),
            glm::vec4(0.7f, 0.7f, 0.7f, 0.1f));
        m_World.emplace<entt::tag<"BounceY"_hs>>(m_Top);
        m_Bottom = addCube(
            "Bottom Container",
            glm::vec3(m_Pos.x + m_Dim.x * 0.5f, m_Pos.y - 1.1f, m_Pos.z + m_Dim.z * 0.5f),
            glm::vec3(m_Dim.x, 1.f, m_Dim.z),
            glm::vec4(0.7f, 0.7f, 0.7f, 0.1f));
        m_World.emplace<entt::tag<"BounceY"_hs>>(m_Bottom);

        m_Front = addCube(
            "Front Container",
            glm::vec3(m_Pos.x + m_Dim.x * 0.5f, m_Pos.y + m_Dim.y * 0.5f, m_Pos.z + m_Dim.z + 1.1f),
            glm::vec3(m_Dim.x, m_Dim.y, 1.f),
            glm::vec4(0.7f, 0.7f, 0.7f, 0.1f));
        m_World.emplace<entt::tag<"BounceZ"_hs>>(m_Front);
        m_Back = addCube(
            "Back Container",
            glm::vec3(m_Pos.x + m_Dim.x * 0.5f, m_Pos.y + m_Dim.y * 0.5f, m_Pos.z - 1.1f),
            glm::vec3(m_Dim.x, m_Dim.y, 1.f),
            glm::vec4(0.7f, 0.7f, 0.7f, 0.1f));
        m_World.emplace<entt::tag<"BounceZ"_hs>>(m_Back);

        m_Right = addCube(
            "Right Container",
            glm::vec3(m_Pos.x + m_Dim.x + 1.1f, m_Pos.y + m_Dim.y * 0.5f, m_Pos.z + m_Dim.z * 0.5f),
            glm::vec3(1.f, m_Dim.y, m_Dim.z),
            glm::vec4(0.7f, 0.7f, 0.7f, 0.1f));
        m_World.emplace<entt::tag<"BounceX"_hs>>(m_Right);
        m_Left = addCube(
            "Left Container",
            glm::vec3(m_Pos.x - 1.1f, m_Pos.y + m_Dim.y * 0.5f, m_Pos.z + m_Dim.z * 0.5f),
            glm::vec3(1.f, m_Dim.y, m_Dim.z),
            glm::vec4(0.7f, 0.7f, 0.7f, 0.1f));
        m_World.emplace<entt::tag<"BounceX"_hs>>(m_Left);

        kawe::Children::emplace(m_World, m_LevelContainer, std::vector<entt::entity>{m_Top, m_Bottom, m_Front, m_Back, m_Right, m_Left});
    }

    auto addCube(
        const std::string &name,
        const glm::vec3 &pos = glm::vec3(0.f),
        const glm::vec3 &scale = glm::vec3(2.f),
        const glm::vec4 &color = glm::vec4(0.7f, 0.7f, 0.7f, 0.5f)) -> entt::entity
    {
        auto lowerScale = scale;
        lowerScale *= 0.5;
        spdlog::info("CheckCOlision lowerScale {}, {}, {}", lowerScale.x, lowerScale.y, lowerScale.z);
        auto cube = m_World.create();

        m_World.emplace<kawe::Name>(cube, name);

        kawe::Mesh::emplace(m_World, cube, "./asset/models/cube/cube.obj");
        m_World.emplace<kawe::FillColor>(cube, color);
        // Fill the color need to enable shader default to work on model
        m_World.patch<kawe::Render::VAO>(cube, [this](kawe::Render::VAO &component) {
            component.shader_program = m_World.ctx<kawe::State *>()->shaders[0].get();
        });

        m_World.emplace<kawe::Scale3f>(cube, lowerScale);
        m_World.emplace<kawe::Position3f>(cube, pos);

        m_World.emplace<kawe::Collider>(cube, kawe::Collider());

        m_World.emplace<entt::tag<"Container"_hs>>(cube);

        return cube;
    }

private:
    entt::registry &m_World;
    glm::vec3 m_Dim;
    glm::vec3 m_Pos;

    entt::entity m_LevelContainer;
    entt::entity m_Top;
    entt::entity m_Bottom;
    entt::entity m_Front;
    entt::entity m_Back;
    entt::entity m_Right;
    entt::entity m_Left;
};

} // namespace cbk

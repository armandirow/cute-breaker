#pragma once

#include "Engine.hpp"

#include "primitives/Sphere.hpp"

using namespace entt::literals;

namespace cbk {

class Ball {
public:
    Ball(entt::registry &world) : m_World{world}
    {
        m_Entity = create_sphere(m_World, 1, 10, 10);
        m_World.emplace<kawe::Name>(m_Entity, "Ball");
        m_World.emplace<entt::tag<"Ball"_hs>>(m_Entity);
        m_World.emplace<kawe::Collider>(m_Entity);


        m_World.replace<kawe::Position3f>(m_Entity, glm::vec3(70.f, 5.f, 0.f));
        m_World.emplace<kawe::FillColor>(m_Entity, glm::vec4(1.f, 0.5f, 0.5f, 1.f));
        m_World.emplace<kawe::Velocity3f>(m_Entity, glm::vec3(0.f, 0.f, 20.f));

    }

    auto getEntity() const { return m_Entity; }


private:
    entt::entity m_Entity;
    entt::registry &m_World;
};

} // namespace cbk

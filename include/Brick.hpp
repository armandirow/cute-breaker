#pragma once

#include <cstdlib>

#include "Engine.hpp"

using namespace entt::literals;

namespace cbk {

#define GET_RANDOM_FLOAT() static_cast<float>(std::rand()) / (static_cast<float>(RAND_MAX))

class Brick {
public:
    template<typename T>
    struct Mass {
        T mass;
    };

    using MassF = Mass<float>;

    struct LifePoint {
        float maxHealth;
        float health;
    };

public:
    Brick(
        entt::registry &world,
        const std::string &name,
        const glm::vec3 &pos = glm::vec3(0.f),
        const glm::vec3 &dim = glm::vec3(10.f, 10.f, 2.f),
        const glm::vec4 &color = glm::vec4(GET_RANDOM_FLOAT(), GET_RANDOM_FLOAT(), GET_RANDOM_FLOAT(), 1.f),
        const LifePoint &life = {10, 10}) :
        m_World(world),
        m_Name(name), m_Pos(pos), m_Dim(dim), m_Life{life}, m_Color(color)
    {
        m_Brick = m_World.create();

        m_World.emplace<kawe::Name>(m_Brick, m_Name);
        m_World.emplace<Brick::LifePoint>(m_Brick, m_Life);
        m_World.emplace<entt::tag<"Brick"_hs>>(m_Brick);

        m_Top = addCube(
            m_Name + " Top",
            glm::vec3(m_Pos.x + m_Dim.x*0.5f, m_Pos.y + m_Dim.y + 0.1f, m_Pos.z + m_Dim.z*0.5f),
            glm::vec3(m_Dim.x, 0.f, m_Dim.z),
            color);
        m_World.emplace<entt::tag<"BounceY"_hs>>(m_Top);
        m_Bottom = addCube(
            m_Name + " Bottom",
            glm::vec3(m_Pos.x + m_Dim.x*0.5f, m_Pos.y - 0.1f, m_Pos.z + m_Dim.z*0.5f),
            glm::vec3(m_Dim.x, 0.f, m_Dim.z),
            color);
        m_World.emplace<entt::tag<"BounceY"_hs>>(m_Bottom);

        m_Front = addCube(
            m_Name + " Front",
            glm::vec3(m_Pos.x + m_Dim.x*0.5f, m_Pos.y + m_Dim.y*0.5f, m_Pos.z + m_Dim.z + 0.1f),
            glm::vec3(m_Dim.x, m_Dim.y, 0.f),
            color);
        m_World.emplace<entt::tag<"BounceZ"_hs>>(m_Front);
        m_Back = addCube(
            m_Name + " Back",
            glm::vec3(m_Pos.x + m_Dim.x*0.5f, m_Pos.y + m_Dim.y*0.5f, m_Pos.z - 0.1f),
            glm::vec3(m_Dim.x, m_Dim.y, 0.f),
            color);
        m_World.emplace<entt::tag<"BounceZ"_hs>>(m_Back);

        m_Right = addCube(
            m_Name + " Right",
            glm::vec3(m_Pos.x + m_Dim.x + 0.1f, m_Pos.y + m_Dim.y*0.5f, m_Pos.z + m_Dim.z*0.5f),
            glm::vec3(0.f, m_Dim.y, m_Dim.z),
            color);
        m_World.emplace<entt::tag<"BounceX"_hs>>(m_Right);
        m_Left = addCube(
            m_Name + " Left",
            glm::vec3(m_Pos.x - 0.1f, m_Pos.y + m_Dim.y*0.5f, m_Pos.z + m_Dim.z*0.5f),
            glm::vec3(0.f, m_Dim.y, m_Dim.z),
            color);
        m_World.emplace<entt::tag<"BounceX"_hs>>(m_Left);

        kawe::Children::emplace(m_World, m_Brick, std::vector<entt::entity>{m_Top, m_Bottom, m_Front, m_Back, m_Right, m_Left});
    }


    auto addCube(
        const std::string &name,
        const glm::vec3 &pos = glm::vec3(0.f),
        const glm::vec3 &scale = glm::vec3(1.f),
        const glm::vec4 &color = glm::vec4(0.7f, 0.7f, 0.7f, 0.5f),
        const LifePoint &life = {10, 10}) -> entt::entity
    {
        auto lowerScale = scale;
        lowerScale *= 0.5;
        auto cube = m_World.create();

        m_World.emplace<kawe::Name>(cube, name);

        kawe::Mesh::emplace(m_World, cube, "./asset/models/cube/cube.obj");
        m_World.emplace<kawe::FillColor>(cube, color);
        // Fill the color need to enable shader default to work on model
        m_World.patch<kawe::Render::VAO>(cube, [this](kawe::Render::VAO &component) {
            component.shader_program = m_World.ctx<kawe::State *>()->shaders[0].get();
        });

        m_World.emplace<kawe::Scale3f>(cube, lowerScale);
        m_World.emplace<kawe::Position3f>(cube, pos);

        m_World.emplace<kawe::Collider>(cube, kawe::Collider());

        m_World.emplace<Brick::LifePoint>(cube, life);
        m_World.emplace<entt::tag<"Brick"_hs>>(cube);

        return cube;
    }

    auto breakWall() -> void
    {
        const float ballDamage = 10.f;
        const auto &life = m_World.try_get<Brick::LifePoint>(m_Brick);
        if (life) {
            m_World.patch<Brick::LifePoint>(m_Brick, [&ballDamage](auto &l) {
                l.health -= ballDamage;
            });
        }
        if (life && life->health <= 0) {
            m_World.remove_all(m_Brick);
            m_World.remove_all(m_Top);
            m_World.remove_all(m_Bottom);
            m_World.remove_all(m_Front);
            m_World.remove_all(m_Back);
            m_World.remove_all(m_Right);
            m_World.remove_all(m_Left);
        }
    }

    auto getEntity() const { return m_Brick; }

private:
    entt::registry &m_World;
    std::string m_Name;
    glm::vec3 m_Pos;
    glm::vec3 m_Dim;
    LifePoint m_Life;
    glm::vec4 m_Color;

    entt::entity m_Brick;
    entt::entity m_Top;
    entt::entity m_Bottom;
    entt::entity m_Front;
    entt::entity m_Back;
    entt::entity m_Right;
    entt::entity m_Left;
};

} // namespace cbk

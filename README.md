Le projet est découpé en deux partie
- [BACK](#BACK)
- [FRONT](#FRONT)

Le projet est lancé avec un docker compose

## BACK

le backend est composé de 2 partie
 - [API](#API)
 - [WORKER](#WORKER)

Le service redis est utilisé pour la communication entre le worker et l'api
Le worker et l'api sont en **python** **3.8** et utilise la bibliothèque [celery
](https://docs.celeryq.dev/en/stable/getting-started/introduction.html) en version 

En développement de l'application et par l'intermédiaire du **docker-compose** le worker et l'api sont en auto-reload. C'est à dire que si un changement a lieu dans les fichier python alors l'application prendra compte de ces changements sans avoir a rebuild l'image docker.

| ⚠️ WARNING |
| :------ |
| Si les modifications sont en rapport avec les packages python il faut rebuild l'image |

Le service worker et api communique aussi des fichier via le répertoire `./back/tmp`

<pre>
Ex: Lors d'un transfert d'un fichier du front vers le back,  le service api reçoit le fichier, si le service <b>worker</b> a besoin de ce fichier alors il est enregistré dans ce répertoire. Le service <b>api</b> crée ensuite une tache qui sera exécuté par le service <b>worker</b> qui pourra l'ouvrir en <b>mémoire</b> dans son conteneur et ensuite supprimé le fichier temporaire.
</pre>

| ℹ️ **INFO** |
| :------ |
| Les logs des taches effectués par le worker sont stocké dans le fichier **celery.log** situé dans le répertoire `./back/logs` |

### API

Pour le service **API**, nous utilisons la library **fastapi**.
Ce service récupère les demandes du front pour ensuite communiquer avec le service **worker** qui s'occupe de faire les calculs.
Le service **API** s’occupe aussi de retourner les résultats d'une tache fini du service **worker** sous la forme suivante :

```json
result = {
	"task_id": number,
	"task_status": "FAILURE"|"PENDING"|"RECEIVED"|"RETRY"|
		"REVOKED"|"STARTED"|"SUCCESS",
	"task_result": any
}
```

En appelant la route suivante : `GET` `/tasks/{task_id}`

### WORKER

Le service worker est une file d'attente et un exécuteur de taches.
Car les taches peuvent prendre un certain temps, donc on ne veut pas bloquer le front en lui faisant attendre les résultat de la tache, c'est pour ça que le front passe par l'api pour recevoir les résultat.

pour déclarer une nouvelle tache on peut faire comme cela dans le fichier `worker.py`

```python
@celery.task(name="task_name")
def task_name(arg)
	do something
	return something
```

Pour la logique de la tache, on gardera celle la dans d'autre fichier pour garder une meilleure visibilité.

Ex: pour **eda** -> la logique est dans le dossier `src/augment_lib` 
## FRONT

La partie front est basé sur ReactJS en mode typescript.

On utilise yarn pour la gestion de packets

On utilise les framework suivant:
- [MUI](https://mui.com/) permet d'avoir des component déja designé
- [Tailwind CSS](https://tailwindcss.com/) permet d'écrire du css plus rapidement dans les fichiers ts sous forme de classe
- [axios](https://axios-http.com/docs/intro) pour la communication avec le back via l'API
- [jszip](https://stuk.github.io/jszip/) gestion des fichiers zip reçu par le back
- [papaparse](https://www.papaparse.com/) parsing des fichiers CSV
- [read-excel-file](https://www.npmjs.com/package/read-excel-file) parsing des fichiers excel
- [dataframe-js](https://gmousse.gitbooks.io/dataframe-js/content/) Manipulation de data sous format dataframe
- [chart.js](https://www.chartjs.org/) création d'affichage sous format graphique/chart
- [primereact](https://primereact.org/) utilisé pour afficher les notifications sous format toast

on utilise aussi **eslint** et **prettier** pour avoir un code uniforme et propre

[to front](#FRONT)



